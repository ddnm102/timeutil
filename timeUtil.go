package timeutil

import "time"

//InTimeSpan Check time check between start and end time
func InTimeSpan(start, end, check time.Time) bool {
	//|----------(start)----------------------(end)--------------->
	//|---(check)-------------------------------------------------> => False
	//|------------------------(check)----------------------------> => True
	//|------------------------------------------------(check)----> => False
	//Kiểm tra check có nằm ở phía sau của start hay không.
	a := check.After(start) || check.Equal(start)
	//Kiểm tra check có nằm ở phía trước của start hay không
	b := check.Before(end) || check.Equal(end)
	//return check.After(start) && check.Before(end)
	return a && b
}
